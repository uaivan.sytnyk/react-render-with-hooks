import { useEffect, useState} from "react";

const useFetchMovies = (link) => {

    const [films, fetchPopularFilms] = useState([]);
    const [totalPages, setTotal] = useState(1);
    const [page, setPage] = useState(1);
    
    useEffect(() => {
        const fetching = async (page) => {

            const responce = await fetch(`${link}&page=${page}`)
            const data = await responce.json();
    
            fetchPopularFilms(data.results);
            setTotal(data.total_pages);
        };
        fetching(page);
    }, [page]);

    return  {data: films, total: totalPages, currentPage: page, setPage};
}

export default useFetchMovies;
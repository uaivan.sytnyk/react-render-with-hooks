import React, { useContext } from "react";
import './Pagination.css'
import { StoreContext } from "../Store";

const Pagination = ({ page, totalPages, changePage }) => {
  const context = useContext(StoreContext);
  return (

    <div className={context.theme === "light" ? "Pagination" : "PaginationDark"}>
      <div>Current Page: {page}/{totalPages}</div>

      <button
        onClick={() => changePage(page - 1)}
        disabled={page === 1}
      >
        Previous Page
      </button>

      <button
        onClick={() => {
          changePage(page + 1);
        }}
        //disabled={page === totalPages}
        disabled={page === 500}
      >
        Next Page
      </button>
    </div>
  );
}

export default Pagination;
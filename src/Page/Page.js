import { useContext, useState } from "react";
import Pagination from "./Pagination";
import Header from "./Header";
import List from "./List";
import PopUp from "../components/PopUp";
import { StoreContext } from "../Store";
import './Page.css'
import useFetchMovies from "../useFetchMovies";

const Page = () => {
  const [selectedFilm, handleFilm] = useState(null);
  const context = useContext(StoreContext);
  const link = `https://api.themoviedb.org/3/movie/popular?api_key=9ab4e0c0c4d3ba62a8ae20bc1aaa38f1`
  const data = useFetchMovies(link);

  return (
    <div className={context.theme === "light" ? "Page" : "DarkPage"}>
      <Header />
      <Pagination
        changePage={data.setPage}
        totalPages={data.total}
        page={data.currentPage}
      />
      <List films={data.data} selectFilm={handleFilm} />
      {selectedFilm && (
        <PopUp date={selectedFilm} closePopUp={() => handleFilm(null)} />
      )}
    </div>
  );
}

export default Page;
import React, { useContext } from "react";
import ThemedButton from "../components/ThemedButton.js";
import './Header.css'
import { StoreContext } from "../Store.js";

const Header = () => {
  const context = useContext(StoreContext)
  return (
    <header className={context.theme === "light" ? "Header" : "DarkHeader"}>
      <h1>Favourite Movies</h1>
      <ThemedButton />
    </header>
  );
}

export default Header;
import React from "react";
import Film from "../components/Film.js";
import RateSwitch from "../components/RateSwitch.js";
import './List.css'

const List = ({ films, selectFilm }) => (
  <ul>
    {
      films.map(film => (
        <Film selectFilm={selectFilm} key={film.id} title={film.original_title} date={film.release_date} poster={film.poster_path} overview={film.overview}>
          <RateSwitch popularity={film.popularity} />
        </Film>
      ))
    }
  </ul>
);

export default List;
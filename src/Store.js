import React from "react";
import { useState } from "react";

export const StoreContext = React.createContext();
const StoreContextProvider = StoreContext.Provider;
export const StoreContextConsumer = StoreContext.Consumer;

const Store = (props) => {
  const storeInformartion = {
    theme: 'light',
    changeTheme: (property, value) => {
      setStoreInfo({ ...storeInfo, [property]: value });
    },
  };

  const [storeInfo, setStoreInfo] = useState(storeInformartion);

  return (
    <StoreContextProvider value={storeInfo}>
      {props.children}
    </StoreContextProvider>
  );
}

export default Store;
import React from "react";
import './PopUp.css'

const PopUp = ({ date, closePopUp }) => (
  <div className="PopUp">
    <span
      className="close-btn"
      onClick={closePopUp}>
      &times;
    </span>
    <p>{date}</p>
  </div>
);

export default PopUp;
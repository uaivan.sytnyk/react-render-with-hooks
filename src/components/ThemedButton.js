import React, { useContext } from "react";
import sunPath from '../images/sun.svg'
import moonPath from '../images/moon.svg'
import { StoreContext } from "../Store";
import './ThemedButton.css';

const body = document.getElementsByTagName("BODY")[0];

const ThemedButton = () => {
  const context = useContext(StoreContext);

  context.theme === 'light' ? body.style.backgroundColor = `` : body.style.backgroundColor = `rgb(19, 25, 37)`;

  return (
    <div
      onClick={() => context.changeTheme('theme', context.theme === "light" ? "dark" : "light")}
      className={context.theme === "light" ? "svg" : "svg-dark"}
      >
      {context.theme === "light" ? <img src={moonPath} alt="Moon" />
        : <img src={sunPath} alt="Sun" />
      }
    </div>
  );
}

export default ThemedButton;
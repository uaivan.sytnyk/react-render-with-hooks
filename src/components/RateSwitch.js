import React, { useState } from "react";
import './Switch.css'

const RateSwitch = ({ popularity }) => {
  const [toggle, setToogle] = useState(true);

  return (
    <p onClick={() => setToogle(!toggle)} className="switch">
      {toggle ? "Show Rate" : `${popularity} Hide Rate`}
    </p>
  );
}

export default RateSwitch;
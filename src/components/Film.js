import React, {useContext} from "react";
import './Film.css'
import { StoreContext } from "../Store";

const Film = ({ title, poster, overview, children, date, selectFilm }) => {
  const contex = useContext(StoreContext);
  return (
    <li className={contex.theme === "light" ? "Film" : "DarkFilm"} >
      <img className='image'
        src={
          poster ? `https://www.themoviedb.org/t/p/w200${poster}`
            : "https://wellness-psychiatry.com/wp-content/uploads/2016/12/noimage-200x300.jpg"
        }
        alt='poster'
        onClick={() => { selectFilm(date) }}
      />
      <div>
        <h1>{title}</h1>
        <p>{overview}</p>
        {children}
      </div>
    </li>
  );
}

export default Film;